Source: pytest-catchlog
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 python-all,
 python-py,
 python-pygal,
 python-setuptools,
 python-pytest,
 python3-all,
 python3-py,
 python3-pygal,
 python3-setuptools,
 python3-pytest,
 pandoc
Standards-Version: 4.1.0
Homepage: https://github.com/eisensheng/pytest-catchlog
Vcs-Git: https://salsa.debian.org/python-team/packages/pytest-catchlog.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pytest-catchlog

Package: python-pytest-catchlog
Architecture: all
Depends:
 python-pytest,
 ${misc:Depends},
 ${python:Depends}
Description: py.test plugin to capture test log messages
 By default each captured log message shows the module, line number, log level
 and message. Showing the exact module and line number is useful for testing
 and debugging. If desired the log format and date format can be specified to
 anything that the logging module supports.
 .
 This is a fork of pytest-capturelog by Meme Dough.
 .
 The package contains the plugin for Python 2 code.

Package: python3-pytest-catchlog
Architecture: all
Depends:
 python3-pytest,
 ${misc:Depends},
 ${python3:Depends}
Description: py.test plugin to capture test log messages (Python3)
 By default each captured log message shows the module, line number, log level
 and message. Showing the exact module and line number is useful for testing
 and debugging. If desired the log format and date format can be specified to
 anything that the logging module supports.
 .
 This is a fork of pytest-capturelog by Meme Dough.
 .
 The package contains the plugin for Python 3 code.
